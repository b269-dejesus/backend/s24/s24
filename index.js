// ES6 Updates Activity S24

// console.log(`Hello Mader Fader!`);
	

let num1 = Number(prompt(`Enter a Number`));
const getCube = num1 ** 3;

console.log(`The cube of ${num1} is ${getCube} `);

////////////////

const address = ['No.143' , 'Gandania' , 'Taguig' , 'Philippines'];
const  [streetNo , barangay , city , country] = address

console	.log(`I live at ${streetNo} ${barangay}, ${city}, ${country}`);


const 	animal = {
		animalName	: 'Paaka',
		animalType	: 'Dog',
		weight		:'13 kgs',
		length		:'3 ft.'
		
}

const {animalName , animalType , weight , length} = animal;

console	.log(`${animalName} is a very smart ${animalType}. He weighted at ${weight}, and with a size ${length}`);


const numbers = [1 , 2 , 3 , 4 , 5];


numbers.forEach((number) =>{
	// let sum = numbers+numbers//
	console.log (`${number}`)
	
});


const number1 = 0;
const sum = numbers.reduce((numStart, numCurrent) => 
  numStart + numCurrent, number1
);

console.log(sum);




class Dog {
	constructor(name , age , breed){
		this.name	= name;
		this.age	= age;
		this.breed	= breed;

	}
}



const firstDog = new Dog ("ADA" , 5 ,  "German Sheperd" );
console.log(firstDog);		


const secondDog = new Dog ("Coco" , 6 ,  "Chow Chow" );
console.log(secondDog);		
